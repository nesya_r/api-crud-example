module.exports = function (sequelize, DataTypes) {
  return sequelize.define('package', {
    packag: DataTypes.STRING,
    descr: DataTypes.STRING,
  }, {
    classMethods: {

    },
    instanceMethods: {
      toJSON: function () {
        let values = Object.assign({}, this.get());
        return values;
      }
    },
    underscored: true,
    timestamps: false,
    tableName: 'package'
  })
};
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('auditorder', {
    changedstatus: DataTypes.STRING,
    users: DataTypes.STRING,
    dsc: DataTypes.STRING,
    created: DataTypes.DATE,
    pic: DataTypes.STRING
  }, {
    classMethods: {

    },
    instanceMethods: {
      toJSON: function () {
        let values = Object.assign({}, this.get());
        return values;
      }
    },
    underscored: true,
    timestamps: false,
    tableName: 'auditorder',
    freezeTableName: true
  })
};
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('customerdata', {
    fullname: {
      type: DataTypes.STRING,
      get() {
        return this.getDataValue('fullname').trim();
      }
    },
    email: {
      type: DataTypes.STRING,
      get() {
        return this.getDataValue('email').trim();
      }
    },
    telepon: {
      type: DataTypes.STRING,
      get() {
        return this.getDataValue('telepon').trim();
      }
    },
    alamat: {
      type: DataTypes.TEXT,
      get() {
        return this.getDataValue('alamat').trim();
      }
    }
  }, {
    classMethods: {

    },
    instanceMethods: {
      toJSON: function () {
        let values = Object.assign({}, this.get());
        return values;
      }
    },
    underscored: true,
    timestamps: false,
    tableName: 'customerdata',
    freezeTableName: true
  })
};
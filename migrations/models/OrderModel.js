module.exports = function (sequelize, DataTypes) {
  return sequelize.define('order', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    status_order: DataTypes.STRING,
    shipping_method: DataTypes.STRING,
    paymentmethod: DataTypes.STRING,
    nomtransfer: DataTypes.BIGINT,
    platform: DataTypes.BIGINT,
    updateby: DataTypes.STRING,
    createdby: DataTypes.STRING,
    creat: DataTypes.DATE,
    lastupdate: DataTypes.DATE,
    isdelete: DataTypes.ENUM('0', '1'),
    warning: DataTypes.ENUM('0', '1'),
    descript: DataTypes.TEXT,
    payment_status: DataTypes.STRING
  }, {
      classMethods: {

      },
      instanceMethods: {
        toJSON: function () {
          let values = Object.assign({}, this.get());
          return values;
        }
      },
      underscored: true,
      timestamps: false
    })
};
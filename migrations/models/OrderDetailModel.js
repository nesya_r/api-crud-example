module.exports = function (sequelize, DataTypes) {
  return sequelize.define('orderdetail', {
    cover: DataTypes.STRING,
    qty: DataTypes.INTEGER,
    updateby: DataTypes.STRING,
    createdby: DataTypes.STRING,
    creat: DataTypes.DATE,
    lastupdate: DataTypes.DATE,
    isdelete: DataTypes.ENUM('0', '1'),
  }, {
    classMethods: {

    },
    instanceMethods: {
      toJSON: function () {
        let values = Object.assign({}, this.get());
        return values;
      }
    },
    underscored: true,
    timestamps: false
  })
};
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING,
    telp: DataTypes.STRING,
    role: DataTypes.STRING,
  }, {
    classMethods: {

    },
    instanceMethods: {
      toJSON: function () {
        let values = Object.assign({}, this.get());
        return values;
      }
    },
    underscored: true,
    timestamps: false
  })
};
const OldOrder = require('./models').order;
const OldOrderDetail = require('./models').orderdetail;
const OldAuditOrder = require('./models').auditorder;
const OldUserModel = require('./models').user;
const OldCustomer = require('./models').customerdata;

const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;
const StatusModel = require('../app/models').status_type;
const OrderModel = require('../app/models').order;
const Customer = require('../app/models').customer;
const CustomerAddress = require('../app/models').customer_address;
const PackageOrder = require('../app/models').package_order;
const PhotobookStatusModel = require('../app/models').photobook_status;
const bcrypt = require('bcrypt');

var ProgressBar = require('progress');
const Promise = require('bluebird');

PhotobookStatusModel.removeHook('afterCreate', 'afterBulkCreate');
OrderModel.removeHook('afterCreate')

OldOrder.findAndCountAll({
  include: ['orderdetail', {
    model: OldCustomer
  }],
  where: {
    isdelete: '0'
  },
}).then(oldOrders => {
  console.log(oldOrders);
  var bar = new ProgressBar(':bar', {
    total: oldOrders.count
  });

  const allData = [];
  Promise.reduce(oldOrders.rows, (accumulator, order, index, key) => {
    return new Promise((resolve, reject) => {
      const UserStatusPromise = [];
      UserStatusPromise.push(UserModel.findOne({
        where: {
          full_name: order.createdby
        }

      }));
      UserStatusPromise.push(StatusModel.findOne({
        where: {
          status: order.status_order
        }
      }));

      UserStatusPromise.push(
        new Promise((resolve, reject) => {
          const newCustomer = {
            name: order.customerdatum.fullname,
            email: order.customerdatum.email,
            phone: order.customerdatum.telepon,
            customer_addresses: [{
              name: 'Alamat Rumah',
              recipient_name: order.customerdatum.fullname,
              address: order.customerdatum.alamat,
            }]
          }
          Customer.create(newCustomer, {
            include: [CustomerAddress]
          }).then(newCustomer => {
            resolve(newCustomer);
          });
        })
      );

      Promise.all(UserStatusPromise).then(data => {

        const newOrder = {
          order_code: order.id,
          grand_total: order.nomtransfer,
          customer_id: data[2].id,
          customer_address_id: data[2].customer_addresses[0].id,
          created_at: order.creat,
          payment_method: order.paymentmethod,
          platform: order.platform,
          package_orders: [{
            package_id: order.orderdetail.package_id,
            desc: order.descript,
          }]
        }
        if (data[0] !== null) {
          newOrder.cs = data[0].id;
        }
        OrderModel.create(newOrder, {
          include: [PackageOrder]
        }).then(newOrder => {
          bar.tick();
          resolve(newOrder)
        })
      }).catch(err => {
        bar.tick();
        console.log(order.toJSON());
        reject(err);
      });
    });
  }, []).then((result) => {
    if (bar.complete) {
      console.log('completed');
      process.exit();
    }
  }).catch(err => {
    console.log(err);
  });
})

const Photobook = require('../app/models').photobook;
const PhotobookStatus = require('../app/models').photobook_status;

const ProgressBar = require('progress');
let bar = {};

Photobook.findAll({
  where: {
    resi: null
  },
  include: [{ model: PhotobookStatus }],
})
  .then(photobooks => {
    bar = new ProgressBar(':bar', {
      total: photobooks.length
    });
    updatePhotobook(photobooks)
  });

async function updatePhotobook(photobooks) {
  for (let photobook of photobooks) {
    for (let photobook_status of photobook.photobook_statuses) {
      if (photobook_status.status_type_id == 16 && photobook_status.desc != '') {
        photobook.resi = photobook_status.desc;
        await photobook.save();
      }
    }
    bar.tick();
  }
  if (bar.complete) {
    console.log('completed');
    process.exit();
  }
}
const OldOrder = require('./models').order;
const OldOrderDetail = require('./models').orderdetail;
const OldAuditOrder = require('./models').auditorder;
const OldUserModel = require('./models').user;
const OldCustomer = require('./models').customerdata;

const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;
const StatusModel = require('../app/models').status_type;
const OrderModel = require('../app/models').order;
const Customer = require('../app/models').customer;
const CustomerAddress = require('../app/models').customer_address;
const PackageOrder = require('../app/models').package_order;
const PhotobookModel = require('../app/models').photobook;
const PhotobookStatus = require('../app/models').photobook_status;
const Package = require('../app/models').package;

const bcrypt = require('bcrypt');
var ProgressBar = require('progress');
const Promise = require('bluebird');
const _ = require('lodash');
const moment = require('moment');


const rejectedStatus = ['Cek kreo ulang', 'Cek ulang pdf', 'Data tidak lengkap', 'Foto bermasalah', 'Layout ulang', 'Print ulang'];

PhotobookStatus.removeHook('afterCreate', 'afterBulkCreate');
OrderModel.removeHook('afterCreate')

var myArgs = process.argv.slice(2);

try {
  const offset = parseInt(myArgs[0]);
  console.log('Migrasi data ' + (offset + 1) + ' sampai ' + (offset + 1000));
  migrate(offset);
} catch (error) {
  console.log(error);
}
async function migrate(offset) {
  console.log("Calculating....");

  const oldOrders = await OldOrder.findAll({
    where: {
      isdelete: '0'
    }
  });

  var bar = new ProgressBar(':bar', {
    total: oldOrders.length
  });

  console.log("Start Migrating");
  console.log(oldOrders.length);

  for (let order of oldOrders) {
    await OrderModel.update({
      shipment_method: order.shipping_method
    }, {
      where: {
        order_code: order.id
      }
    });
    bar.tick();
    if (bar.complete) {
      console.log("Completed");
      process.exit();
    }
  }
}
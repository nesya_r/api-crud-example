const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;
const StatusModel = require('../app/models').status_type;
const OrderModel = require('../app/models').order;
const Customer = require('../app/models').customer;
const CustomerAddress = require('../app/models').customer_address;
const PackageOrder = require('../app/models').package_order;
const PhotobookStatusModel = require('../app/models').photobook_status;
const PhotobookModel = require('../app/models').photobook;

var ProgressBar = require('progress');

PhotobookStatusModel.removeHook('afterCreate');
PhotobookStatusModel.removeHook('afterBulkCreate');
OrderModel.removeHook('afterCreate');

var myArgs = process.argv.slice(2);
var limit = 15000;

try {
  const offset = parseInt(myArgs[0]);
  console.log('Migrasi data ' + (offset + 1) + ' sampai ' + (offset + limit));
  migrate(limit, offset);
} catch (error) {
  console.log(error);
}

async function migrate(limit, offset) {

  try {
    const photobooks = await PhotobookModel.findAll({
      include: [
        {
          model: PhotobookStatusModel,
        }
      ],
      offset: offset,
      limit: limit,
      order: [[{ model: PhotobookStatusModel }, 'created_at', 'DESC']],
    });

    var bar = new ProgressBar(':bar', {
      total: photobooks.length
    });

    for (let photobook of photobooks) {
      photobook.pic = photobook.photobook_statuses[0].user_id;
      await photobook.save();
      bar.tick();
    }

    if (bar.complete) {
      console.log('completed');
      process.exit();
    }
  } catch (error) {
    console.log(error);
  }
}

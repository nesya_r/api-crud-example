const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;
const StatusModel = require('../app/models').status_type;
const OrderModel = require('../app/models').order;
const Customer = require('../app/models').customer;
const CustomerAddress = require('../app/models').customer_address;
const PackageOrder = require('../app/models').package_order;
const PhotobookStatusModel = require('../app/models').photobook_status;
const Photobook = require('../app/models').photobook;
const bcrypt = require('bcrypt');
const moment = require('moment');

var ProgressBar = require('progress');
const Promise = require('bluebird');

PhotobookStatusModel.removeHook('afterCreate', 'afterBulkCreate');
OrderModel.removeHook('afterCreate')
console.log("Calculating....")
let bar = {};
var myArgs = process.argv.slice(2);
const offset = parseInt(myArgs[0]);
Photobook.findAll({
  include: [{
    model: PhotobookStatusModel,
  }],
  limit: 10000,
  offset: offset,
  order: [[{ model: PhotobookStatusModel }, 'created_at', 'DESC']]
}).then(photobooks => {
  bar = new ProgressBar(':bar', {
    total: photobooks.length
  });
  migrate_status(photobooks);
}).catch(err => {
  console.log(err);
});

async function migrate_status(photobooks) {
  for (const photobook of photobooks) {
    await Photobook.update({
      current_status: photobook.photobook_statuses[0].status_type_id,
      status_updated_at: photobook.photobook_statuses[0].created_at
    }, {
        where: {
          id: photobook.id
        }
      });
    bar.tick();
  }
  process.exit();
}
const OldOrder = require('./models').order;
const OldOrderDetail = require('./models').orderdetail;
const OldAuditOrder = require('./models').auditorder;
const OldUserModel = require('./models').user;

const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;

const bcrypt = require('bcrypt');

OldUserModel.findAll().then(users => {
  const promises = [];
  users.forEach(function (user) {
    promises.push(new Promise((resolve, reject) => {
      RoleModel.findOne({
        where: {
          name: user.role
        }
      }).then(role => {
        if (role) {
          let salt = bcrypt.genSaltSync(10);
          const username = user.name.toLowerCase();
          const password = user.password.replace("$2y$", "$2a$");
          const newUser = {
            username: username,
            password: password,
            full_name: user.name,
            email: user.username,
            phone: user.telp,
            role_id: role.id
          }
          return UserModel.create(newUser)
        } else {
          return new Promise((resolve, reject) => {
            resolve("role kosong")
          });
        }
      }).then(user => {
        resolve(user)
      }).catch(err => {
        reject(err);
      })
    }));
  }, this);

  Promise.all(promises).then(() => {
    console.log("DONE");
  });
});
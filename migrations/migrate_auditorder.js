const OldOrder = require('./models').order;
const OldOrderDetail = require('./models').orderdetail;
const OldAuditOrder = require('./models').auditorder;
const OldUserModel = require('./models').user;
const OldCustomer = require('./models').customerdata;

const RoleModel = require('../app/models').role;
const UserModel = require('../app/models').user;
const StatusModel = require('../app/models').status_type;
const OrderModel = require('../app/models').order;
const Customer = require('../app/models').customer;
const CustomerAddress = require('../app/models').customer_address;
const PackageOrder = require('../app/models').package_order;
const PhotobookModel = require('../app/models').photobook;
const PhotobookStatus = require('../app/models').photobook_status;
const Package = require('../app/models').package;

const bcrypt = require('bcrypt');
var ProgressBar = require('progress');
const Promise = require('bluebird');
const _ = require('lodash');
const moment = require('moment');


const rejectedStatus = ['Cek kreo ulang', 'Cek ulang pdf', 'Data tidak lengkap', 'Foto bermasalah', 'Layout ulang', 'Print ulang'];

PhotobookStatus.removeHook('afterCreate', 'afterBulkCreate');
OrderModel.removeHook('afterCreate')

var myArgs = process.argv.slice(2);

try {
  const offset = parseInt(myArgs[0]);
  console.log('Migrasi data ' + (offset + 1) + ' sampai ' + (offset + 10000));
  migrate(offset);
} catch (error) {
  console.log(error);
}
async function migrate(offset) {
  console.log("Calculating....");
  const oldOrders = await OldOrder.findAll({
    include: [{
      model: OldAuditOrder
    }, {
      model: OldOrderDetail,
      order: [
        ['created', 'ASC']
      ]
    }],
    where: {
      isdelete: '0'
    },
    limit: 10000,
    offset: offset
  });

  console.log("Start Migrating");
  console.log(oldOrders.length);

  var bar = new ProgressBar(':bar', {
    total: oldOrders.length
  });
  for (let order of oldOrders) {

    let start_production = null;
    let status_updated_at = null;
    let is_ever_rejected = 0;
    let is_remake = 0;
    let resi = null;
    let current_status = null;

    status_updated_at = order.auditorders[order.auditorders.length - 1].created;
    current_status = await StatusModel.findOne({
      where: {
        status: _.trim(order.auditorders[order.auditorders.length - 1].changedstatus)
      }
    });

    const cs = await UserModel.findOne({
      where: {
        full_name: order.createdby
      }
    });

    if (!cs) {
      console.log(order.createdby);
    }

    for (let audit of order.auditorders) {
      if (!_.includes([4, 3, 2, 1, 16, 18, 19], current_status.id)) {
        if (audit.changedstatus === 'Foto lengkap') {
          start_production = audit.created
        }
      }

      if (_.includes(rejectedStatus, audit.changedstatus) && is_ever_rejected === 0) {
        is_ever_rejected = 1
      }

      if (audit.changedstatus === 'Complaint' && is_remake === 0) {
        is_remake = 1
      }

      if (audit.changedstatus === 'Terkirim') {
        resi = audit.dsc
      }
    }

    if (!current_status) {
      console.log("current status ", order.auditorders[order.auditorders.length - 1].changedstatus)
    }
    const newOrder = await OrderModel.findOne({
      where: {
        order_code: order.id
      },
      include: [{
        model: PackageOrder
      }]
    });

    if (!newOrder) {
      continue;
    }

    const packagePhotobook = await Package.findOne({
      where: {
        id: order.orderdetail.package_id
      }
    });

    for (var i = 0; i < (order.orderdetail.qty * packagePhotobook.qty); i++) {
      if (!newOrder.package_orders[0]) {
        console.log(newOrder.toJSON());
        console.log("package order null");
      }
      const photobook = {
        start_production: start_production,
        status_updated_at: status_updated_at,
        is_ever_rejected: is_ever_rejected,
        is_remake: is_remake,
        current_status_color: getColor(start_production, status_updated_at, is_ever_rejected, is_remake, current_status),
        resi: resi,
        created_at: order.creat,
        updated_at: order.lastupdate,
        package_order_id: newOrder.package_orders[0].id,
        cover: order.orderdetail.cover,
        current_status: current_status.id,
        photobook_statuses: [],
        cs: (cs) ? cs.id : 55
      };

      const photobook_statuses = [];

      for (let audit of order.auditorders) {
        const user = await getUserByName(audit.users);
        const status = await StatusModel.findOne({
          where: {
            status: audit.changedstatus
          }
        });
        const photobook_status = {
          desc: audit.dsc,
          created_at: audit.created,
          updated_at: audit.created,
        }
        if (user) {
          photobook_status.user_id = user.id
        }

        if (status) {
          photobook_status.status_type_id = status.id
        }
        if (audit.changedstatus == "Masuk print") {
          photobook_status.status_type_id = 11
        }
        if (audit.changedstatus == "Ready to finishing") {
          photobook_status.status_type_id = 13
        }

        if (audit.changedstatus == "Sedang layout") {

          const layouter = await UserModel.findOne({
            where: {
              full_name: audit.users
            }
          });

          if (!layouter) {
            photobook.layouter = 55;
          } else {
            photobook.layouter = layouter.id;
          }

        }
        photobook_statuses.push(photobook_status);
      }
      photobook.photobook_statuses = photobook_statuses;
      const newPhotobook = await PhotobookModel.create(photobook, {
        include: [PhotobookStatus]
      });
      bar.tick();
    }
  }
  if (bar.complete) {
    console.log("completed")
  }
  process.exit();
}

function getColor(start_production, status_updated_at, is_ever_rejected, is_remake, current_status) {
  let color = 0;
  const now = moment();
  const qpaTime = moment(status_updated_at);
  const productionTime = moment(start_production);
  const qpaHours = now.diff(qpaTime, 'hours');
  const prodHours = now.diff(productionTime, 'hours');
  if (is_remake == 1) {
    color = 1; // biru
  }
  if (is_ever_rejected == 1) {
    color = 2; // kuning
  }
  if (qpaHours > 24 && (current_status.id != 16 && current_status.id != 18 && current_status.id != 19)) {
    color = 3; // merah
  }
  if (!current_status) {
    console.log("current status null");
  }
  if (prodHours > 240 && (current_status.id != 16 && current_status.id != 18 && current_status.id != 19)) {
    color = 4; // coklat
  }
  return color;
}
async function getUserByName(name) {
  return await UserModel.findOne({
    where: {
      full_name: name
    }
  });
} 

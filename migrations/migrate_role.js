const OldOrder = require('./models').order;
const OldOrderDetail = require('./models').orderdetail;
const OldAuditOrder = require('./models').auditorder;
const UserModel = require('./models').user;

const RoleModel = require('../app/models').role;

UserModel.findAndCountAll({
  group: ['role']
}).then(roles => {
  const promises = [];
  roles.rows.forEach(function (role) {
    const newRole = {
      name: role.role,
      label: role.role
    }
    if (role.role != '') {
      promises.push(RoleModel.create(newRole))
    }
  }, this);

  Promise.all(promises).then(() => {
    console.log("DONE");
    process.exit();
  });
});
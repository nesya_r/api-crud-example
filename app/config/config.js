try {
  var config = require('../../.config');
} catch (e) {
  var configError = 'Could not find config file. Please make sure `.config.js` exists on your root directory.' +
    'You can also create new config from template file `.config.js.example`.';
  throw Error(configError);
}

/**
 * Get port where the application server runs on
 * @returns {string}
 */
function getPort() {
  return config['app']['port'];
}

/**
 * Get host where the application server runs on
 * @returns {string}
 */
function getHost() {
  return config['app']['host'];
}

/**
 * Get environment variable value
 * @param name
 * @param defaultValue
 * @returns {string}
 */
function getEnv(name, defaultValue) {
  return config.env;
}

/**
 * Get NODE_ENV environment variable
 * @returns {string}
 */
function getNodeEnv() {
  var env = config.env;
  return String(env).toLowerCase();
}

/**
 * Get database config
 * @returns {*}
 */
function getDatabaseConfig() {
  return config.database;
}

/**
 * Check whether the current environment is development
 * @returns {boolean}
 */
function isDevelopment() {
  return getNodeEnv() == 'development';
}

/**
 * Check whether the current environment is development
 * @returns {boolean}
 */
function isProduction() {
  return getNodeEnv() == 'production';
}

function getJWTExpired() {
  return config.jwtExpired;
}

function getJWTSecret() {
  return config.jwtSecret;
}

function getUploadPath() {
  return config.uploadPath;
}

function getStaticPath() {
  return config.staticPath;
}

function getBaseUrl() {
  return config.baseUrl;
}

function getMailgunConfig() {
  return config.mailgun;
}

function getGmailConfig() {
  return config.google;
}

function getStaticPath(name) {
  return config.baseUrl + config.staticPath[name];
}

function getRajaongkirKey() {
  return config.rajaongkir;
}

module.exports = {
  getDatabaseConfig,
  getPort,
  getHost,
  getEnv,
  getNodeEnv,
  isDevelopment,
  isProduction,
  getJWTSecret,
  getJWTExpired,
  getUploadPath,
  getStaticPath,
  getBaseUrl,
  getMailgunConfig,
  getGmailConfig,
  getStaticPath,
  getRajaongkirKey
};
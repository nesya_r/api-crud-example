'use strict';
const expressValidator = require('express-validator');
const _ = require('lodash');
const existValidator = require('../validators/helpers/ExistValidator');
const uniqueValidator = require('../validators/helpers/UniqueValidator');
const existValidatorUser = require('../validators/helpers/ExistValidatorUser');
const existValidatorAddOn = require('../validators/helpers/ExistValidatorAddOn');
const existValidatorSticker = require('../validators/helpers/ExistValidatorSticker');
const PhoneNumberSanitizer = require('../validators/sanitizers/PhoneNumberSanitizer');
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const btoa = require('btoa');
const atob = require('atob');

module.exports = expressValidator({
  customValidators: {

    isNameStickerExist: (value) => {
      return existValidatorSticker.existName(value);
    },
    isNameAddOnExist: (value) => {
      return existValidatorAddOn.existName(value);
    },
    isNameAddOnUpdateExist: (req, res, next) => {
      return existValidatorAddOn.existUpdateName();
    },
    isDriverUser: (value, userID) => {
      //req.value = req.value;
      return existValidatorUser.validateDriverUser(value, userID)
    },
    cekDriverUser: (value) => {
      return existValidatorUser.cekDriverUser(value);
    },
    isAdvertiserUser: (value, userID) => {
      //req.value = req.value;
      return existValidatorUser.validateAdvertiserUser(value, userID)
    },
    cekAdvertiserUser: (value) => {
      return existValidatorUser.cekAdvertiserUser(value);
    },
    isRolesName: (value) => {
      if (value === 'Administrator' || value === 'Accounting' || value === 'AccountManager') {
        return true;
      } else {
        return false;
      }
    },
    isEqualPassword: (value, confirmPassword) => {
      if (value === confirmPassword) {
        return true;
      } else {
        return false;
      }
    },
    isPhoneExist: (value) => {
      return existValidatorUser.existPhone(value);

    },
    isFormatPhone: (phone) => {
      return existValidatorUser.formatPhone(phone);
    },
    isFormatBan: (input) => {
      var newData = parseInt(input);
      if (newData === 0 || newData === 1 || newData === 2) {
        return true;
      } else {
        return false;
      }
    },
    isArray: (value) => {
      return Array.isArray(value);
    },
    notEmptyArray: (value) => {
      if (!Array.isArray(value)) {
        return false;
      }

      return value.length != 0;
    },
    gte: (value, num) => {
      return value >= num;
    },
    lte: (value, num) => {
      return value <= num;
    },
    between: (value, min, max) => {
      return (value <= max) && (value >= min);
    },
    dateValidate: (input, format) => {
      return moment(input, format).isValid();
    },
    strictDateValidate: (input, format, nullable) => {
      if (nullable && !input) {
        return true;
      }
      return moment(input, format, true).isValid();
    },
    isExist: (value, column, userID, userType) => {
      return existValidator(value, column, userID, userType);
    },
    isExistArray: (value, table, column, except, cb) => {
      if (!Array.isArray(value)) {
        value = [value];
      }
      let checks = [];
      _(value).forEach(function (val) {
        checks.push(existValidator(table, column, val, except, cb));
      });
      return Promise.all(checks);
    },
    isUnique: (value, column) => {
      return uniqueValidator(value, column);
    },
    isBase64: (input) => {
      if (!input) return true;
      let base64Image = input;
      if (input.indexOf('image/') > -1) {
        base64Image = base64Image.split(',')[1];
      }
      /*
      let regex = new RegExp(/^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/);
      let regex_option = new RegExp(/^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/);
      return regex_option.test(base64Image);
      */
      if (btoa(atob(base64Image)) === base64Image) {
        return true;
      } else {
        return false;
      }
    },
    callbackCheck: (value, service) => {
      return service(value);
    },
    notEmpty: (input) => {
      if (input !== undefined) {
        input = String(input);
        input = input.trim();
        return validator.isLength(input, {
          min: 1
        });
      } else {
        return false;
      }
    },
    isURL: (url) => {
      if (url == '') {
        return true;
      }
      if (!url || url.length >= 2083 || /\s/.test(url)) {
        return false;
      }
      if (url.indexOf('mailto:') === 0) {
        return false;
      }
      const options = {
        protocols: ['http', 'https', 'ftp'],
        require_tld: true,
        require_protocol: false,
        require_valid_protocol: true,
        allow_underscores: false,
        allow_trailing_dot: false,
        allow_protocol_relative_urls: false,
      };
      let protocol, auth, host, hostname, port, port_str, split;

      split = url.split('#');
      url = split.shift();

      split = url.split('?');
      url = split.shift();

      split = url.split('://');
      if (split.length > 1) {
        protocol = split.shift();
        if (options.require_valid_protocol && options.protocols.indexOf(protocol) === -1) {
          return false;
        }
      } else if (options.require_protocol) {
        return false;
      } else if (options.allow_protocol_relative_urls && url.substr(0, 2) === '//') {
        split[0] = url.substr(2);
      }
      url = split.join('://');

      split = url.split('/');
      url = split.shift();
      split = url.split('@');
      if (split.length > 1) {
        auth = split.shift();
        if (auth.indexOf(':') >= 0 && auth.split(':').length > 2) {
          return false;
        }
      }
      hostname = split.join('@');
      split = hostname.split(':');
      host = split.shift();
      if (split.length) {
        port_str = split.join(':');
        port = parseInt(port_str, 10);
        if (!/^[0-9]+$/.test(port_str) || port <= 0 || port > 65535) {
          return false;
        }
      }
      if (!validator.isIP(host) && !validator.isFQDN(host, options) &&
        host !== 'localhost') {
        return false;
      }
      if (options.host_whitelist && options.host_whitelist.indexOf(host) === -1) {
        return false;
      }
      if (options.host_blacklist && options.host_blacklist.indexOf(host) !== -1) {
        return false;
      }
      return true;
    }
  },
  customSanitizers: {
    orderSanitizer: function (value) {
      return OrderIDSanitizer.decode(value);
    },
    phoneSanitizer: function (value) {
      return PhoneNumberSanitizer(value);
    },
    rupiahSanitizer: function (value) {
      return RupiahSanitizer.clear(value);
    },
    toWebsite: function (value) {
      if (!value || value == '') {
        return '';
      }
      if (!value.startsWith('http://') && !value.startsWith('https://')) {
        return 'http://' + value;
      }
      return value;
    }
  },
  errorFormatter: (param, msg, value) => {
    var namespace = param.split('.'),
      root = namespace.shift(),
      formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param: formParam,
      message: msg,
      value: value
    };
  }
});

const _ = require('lodash');
const PaymentCodeModel = require('../models').paymentcode;

function RandomCodeHelper() {

  const generate = (order_id) => {
    return new Promise((resolve, reject) => {
      PaymentCodeModel.findOne({
        where: {
          $or: [
            { order_id: order_id },
            { order_id: null },
          ]
        },
        order: [['order_id', 'DESC'], ['id', 'ASC']]
      })
        .then(code => {
          return code.updateAttributes({ order_id: order_id });
        })
        .then(code => {
          resolve(code);
        })
        .catch(err => {
          reject(err);
        })
    });

  };

  return {
    generate
  };
}


module.exports = RandomCodeHelper();

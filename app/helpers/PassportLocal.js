const Strategy = require('passport-local');

const UserService = require('../services/UserService');

function PassportLocal() {
  return new Strategy(
    (username, password, done) => {
      UserService.authenticate(username, password)
        .then(user => {
          done(null, user);
        }).catch(err => {
          throw err;
          done(null, false);
        });
    });
}
module.exports = PassportLocal;
const _ = require('lodash');

function ArrayHelper() {

    const searchQuery = (filterField, filter) => {
        const queries = [];
        _.forEach(filterField, field => {
            const query = {};
            query[field] = {
                '$like': '%' + filter + '%',
            };
            queries.push(query);
        });
        return queries;
    };

    return {
        searchQuery
    };
}


module.exports = ArrayHelper();

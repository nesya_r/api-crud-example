const moment = require('moment');
const BukuService = require('../services/BukuService');
const BukuModel = require('../models').book;
const JWTService = require('../services/JWTService');

const getBookObj = (req) => {
  return {
    nama_buku: req.input('nama_buku', null),
    penerbit: req.input('penerbit', null),
    penulis: req.input('penulis', null)
    // full_name: req.input('full_name', null),
    // phone: req.input('phone', null),
    // role_id: req.input('role_id', 2000)
  };
};
/**
 * User Controller
 *
 * @constructor
 */
function BukuController() {



  const create = (req, res, next) => {
    const book = getBookObj(req);

    BukuModel.beforeCreate((model, options, done) => {
      model.req = req;
    });
    BukuService.create(book)
      .then(book => {
        req.data = book;
        next();
      })
      .catch(err => {
        next(err);
      });
  };

  const update = (req, res, next) => {
    let id_buku = req.input('id_buku');
    let bookObj = getBookObj(req);
    BukuService.update(id_buku, bookObj).then((book) => {
      req.data = book;
      next();
    }).catch(err => {
      next(err);
    });
  };

  const all = (req, res, next) => {
    BukuService.all()
      .then(books => {
        req.data = books;
        next();
      })
      .catch(err => {
        next(err);
      })
  }

  const allWithPaginate = (req, res, next) => {
    BukuService.allWithPaginate(req.query)
      .then(books => {
        req.data = books;
        next();
      })
      .catch(err => {
        next(err);
      });
  };

  const remove = (req, res, next) => {
    
      let id_bukus = req.input('id_buku', null);
      BukuService.remove(id)
        .then(books => {
          req.data = books;
          next();
        }).catch(err => {
          next(err);
        });
  };

  const byId = (req, res, next) => {
    let id_buku = req.input('id_buku', null);
    BukuService.getById(id_buku)
      .then(book => {
        req.data = book;
        next();
      }).catch(err => {
        next(err);
      });
  };


  return {
    all,
    allWithPaginate,
    create,
    update,
    remove,
    byId
  };
}

module.exports = BukuController();
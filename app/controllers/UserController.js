const moment = require('moment');
const UserService = require('../services/UserService');
const UserModel = require('../models').user;
const JWTService = require('../services/JWTService');

const getUserObj = (req) => {
  return {
    email: req.input('username', null),
    username: req.input('username', null),
    password: req.input('password', null),
    full_name: req.input('full_name', null),
    phone: req.input('phone', null),
    role_id: req.input('role_id', 2000)
  };
};
/**
 * User Controller
 *
 * @constructor
 */
function UserController() {

  const login = (req, res, next) => {
    let username = req.input('username');
    let password = req.input('password');

    var authUser = UserService.authenticate(username, password);
    return authUser.then((user) => {
      var payload = {
        iss: req.hostname,
        exp: moment().add(3600, 'Seconds').valueOf(),
        sub: {
          id: user.get('id'),
          username: user.get('username'),
          full_name: user.get('full_name'),
        },
        iat: moment().unix(),
        jti: 'user'
      };
      req.data = user;
      req.token = JWTService.encode(payload);
      next();
    }).catch(err => {
      throw err;
      next(err);
    });
  };

  const checkUsername = (req, res, next) => {
    const username = req.input('username', null);
    UserService.checkUsername(username)
      .then(user => {
        req.data = user;
        next();
      })
      .catch(err => {
        next(err);
      });
  };

  const create = (req, res, next) => {
    const user = getUserObj(req);

    UserModel.beforeCreate((model, options, done) => {
      model.req = req;
    });
    UserService.create(user)
      .then(user => {
        req.data = user;
        next();
      })
      .catch(err => {
        next(err);
      });
  };

  const update = (req, res, next) => {
    let id = req.input('id');
    let userObj = getUserObj(req);
    UserService.update(id, userObj).then((user) => {
      req.data = user;
      next();
    }).catch(err => {
      next(err);
    });
  };

  const all = (req, res, next) => {
    UserService.all()
      .then(users => {
        req.data = users;
        next();
      })
      .catch(err => {
        next(err);
      })
  }

  const allWithPaginate = (req, res, next) => {
    UserService.allWithPaginate(req.query)
      .then(users => {
        req.data = users;
        next();
      })
      .catch(err => {
        next(err);
      });
  };

  const remove = (req, res, next) => {
    if (req.user.role.id === 2000) {
      let err = new Error("Akses tidak diizinkan");
      err.code = 401;
      next(err);
    } else {
      let id = req.input('id', null);
      UserService.remove(id)
        .then(user => {
          req.data = user;
          next();
        }).catch(err => {
          next(err);
        });
    }
  };

  const byId = (req, res, next) => {
    let id = req.input('id', null);
    UserService.getById(id)
      .then(user => {
        req.data = user;
        next();
      }).catch(err => {
        next(err);
      });
  };

  const withCustomer = (req, res, next) => {
    let id = req.input('id', null);
    UserService.getByIdWithCustomer(id)
      .then(user => {
        if (req.user.id != user.id) {
          let error = new Error("Akses data tidak diizinkan");
          error.code = '404';
          next(error);
        } else {
          req.data = user;
          next();
        }
      }).catch(err => {
        next(err);
      });
  }

  return {
    all,
    allWithPaginate,
    withCustomer,
    checkUsername,
    create,
    update,
    remove,
    byId,
    login
  };
}

module.exports = UserController();
const express = require('express');
const router = express.Router();

const UserController = require('../controllers/UserController');
const UserValidator = require('../validators/UserValidator');
const ResponseMiddleware = require('../middlewares/ResponseMiddleware');

router.get('/', UserController.all, ResponseMiddleware);
router.get('/:id',UserController.byId,ResponseMiddleware);
router.post('/login', UserController.login, ResponseMiddleware);
router.post('/', UserController.create, ResponseMiddleware);
router.put('/', UserController.update, ResponseMiddleware);
router.delete('/:id', UserController.remove, ResponseMiddleware);

module.exports = router;
const express = require('express');
const router = express.Router();

const BukuController = require('../controllers/BukuController');
//const UserValidator = require('../validators/UserValidator');
const ResponseMiddleware = require('../middlewares/ResponseMiddleware');

router.get('/', BukuController.all, ResponseMiddleware);
router.get('/:id',BukuController.byId,ResponseMiddleware);
router.post('/', BukuController.create, ResponseMiddleware);
router.put('/', BukuController.update, ResponseMiddleware);
router.delete('/:id', BukuController.remove, ResponseMiddleware);

module.exports = router;
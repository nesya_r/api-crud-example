module.exports = (express) => {
  let router = express.Router();

  router.use(require('../middlewares/AccessTokenMiddleware')
    .unless({
      path: [
        {
          url: '/',
          methods: ['GET', 'POST']
        }, {
          url: '/users',
          methods: ['POST']
        },
        {
          url: '/users/login',
          methods: ['POST']
        }
      ]
    }));

  router.use('/users', require('./UserRoute'));
  router.use('/books', require('./BukuRoute'));

  return router;
};
const Promise = require('bluebird');
const _ = require('lodash');
const moment = require('moment');
const saltRounds = 10;
const bcrypt = require('bcrypt');
const UserModel = require('../models').user;
const ArrayHelper = require('../helpers/ArrayHelper');
const filteredField = ['username', 'full_name'];

function UserService() {

  const getById = (id) => {

    return new Promise((resolve, reject) => {
      
      UserModel.findOne({
        where: {
          id: id 
        }
      }).then(user => {
        if (user) {
          resolve(user);
        } else {
          let err = Error('Data tidak ditemukan');
          reject(err);
        }
      }).catch(function (err) {
        reject(err);
      });
    });
  };

  const create = (obj) => {
    let data = _.clone(_.omitBy(obj, _.isNil));
    let salt = bcrypt.genSaltSync(saltRounds);
    if (data.password) {
      data.password = bcrypt.hashSync(data.password, salt);
    }

    return new Promise((resolve, reject) => {
      UserModel.create(data)
        .then(newUser => {
          resolve(newUser);
        }, (err) => {
          reject(err);
        });
    });
  };

  const all = () => {
    return new Promise((resolve, reject) => {
      UserModel.findAll({
      })
        .then(users => {
          resolve(users);
        })
        .catch(err => {
          reject(err);
        })
    });
  };

  const allWithPaginate = (pagination) => {
    const limit = parseInt(pagination.limit) || 10;
    const page = parseInt(pagination.page) || 0;
    const filter = pagination.query || '';
    const offset = limit * page;
    return new Promise((resolve, reject) => {
      UserModel.findAndCountAll({
        where: {
          $or: ArrayHelper.searchQuery(filteredField, filter)
        },
        limit: limit,
        offset: offset
      })
        .then(users => {
          resolve(users);
        })
        .catch(err => {
          reject(err);
        })
    });
  };

  const authenticate = (username, password, done) => {
    return new Promise((resolve, reject) => {
      UserModel.findOne({
        where: {
          $or: [{
            username: username
          },
          {
            email: username
          },
          ]
        }
      }).then(function (data) {
        if (data) {
          if (bcrypt.compareSync(password, data.password)) {
            resolve(data);
          } else {
            reject(new Error('Username atau password tidak cocok.'));
          }

        } else {
          reject(new Error('Username atau password tidak cocok.'));
        }
      })
        .catch(function (e) {
          reject(e);
        });
    });
  };

  const checkUsername = (username) => {
    return new Promise((resolve, reject) => {
      UserModel.findOne({
        where: {
          username: username
        }
      }).then(user => {
        resolve(user);
      }).catch(err => {
        reject(err);
      });
    });
  };

  const update = (id, obj) => {
    let data = _.clone(_.omitBy(obj, _.isNil));
    data.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');
    if (data.password) {
      let salt = bcrypt.genSaltSync(saltRounds);
      data.password = bcrypt.hashSync(data.password, salt);
    } else {
      delete data.password;
    }
    // console.log(data);
    return new Promise((resolve, reject) => {
      UserModel.findOne({
        where: {
          id: id
        }
      }).then(user => {
        return user.updateAttributes(data);
      }).then(user => {
        resolve(user);
      }).catch(err => {
        reject(err);
      })
    });

  };

  const remove = (id) => {
    return new Promise((resolve, reject) => {
      UserModel.destroy({
        where: {
          id: id
        }
      }).then(user => {
        resolve(user);
      }).catch(err => {
        reject(err);
      });
    });
  };

  const getByIdWithCustomer = (id) => {
    return new Promise((resolve, reject) => {
      UserModel.findOne({
        where: {
          id: id
        },
        include: ['customer']
      }).then(user => {
        resolve(user);
      }).catch(err => {
        reject(err);
      });
    });
  }

  return {
    getById,
    create,
    authenticate,
    all,
    checkUsername,
    allWithPaginate,
    getByIdWithCustomer,
    update,
    remove
  }
}

module.exports = UserService();
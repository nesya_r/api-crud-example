const Promise = require('bluebird');
const _ = require('lodash');
const moment = require('moment');
const BukuModel = require('../models').book;
const ArrayHelper = require('../helpers/ArrayHelper');

function BukuService() {

  const getById = (id_buku) => {

    return new Promise((resolve, reject) => {
      BukuModel.findOne({
        where: {
          id_buku: id_buku
        }
      }).then(book => {
        if (book) {
          resolve(book);
        } else {
          let err = Error('Data tidak ditemukan');
          reject(err);
        }
      }).catch(function (err) {
        reject(err);
      });
    });
  };

  const create = (obj) => {
    let data = _.clone(_.omitBy(obj, _.isNil));

    return new Promise((resolve, reject) => {
      BukuModel.create(data)
        .then(newBook => {
          resolve(newBook);
        }, (err) => {
          reject(err);
        });
    });
  };

  const all = () => {
    return new Promise((resolve, reject) => {
      BukuModel.findAll({
      })
        .then(books => {
          resolve(books);
        })
        .catch(err => {
          reject(err);
        })
    });
  };

  const allWithPaginate = (pagination) => {
    const limit = parseInt(pagination.limit) || 10;
    const page = parseInt(pagination.page) || 0;
    const filter = pagination.query || '';
    const offset = limit * page;
    return new Promise((resolve, reject) => {
      BukuModel.findAndCountAll({
        where: {
          $or: ArrayHelper.searchQuery(filteredField, filter)
        },
        limit: limit,
        offset: offset
      })
        .then(book => {
          resolve(book);
        })
        .catch(err => {
          reject(err);
        })
    });
  };


  const update = (id_buku, obj) => {
    let data = _.clone(_.omitBy(obj, _.isNil));
    data.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');
  
    return new Promise((resolve, reject) => {
      BukuModel.findOne({
        where: {
          id_buku: id_buku
        }
      }).then(book => {
        return book.updateAttributes(data);
      }).then(book => {
        resolve(book);
      }).catch(err => {
        reject(err);
      })
    });

  };

  const remove = (id_buku) => {
    return new Promise((resolve, reject) => {
      BukuModel.destroy({
        where: {
          id_buku: id_buku
        }
      }).then(book => {
        resolve(book);
      }).catch(err => {
        reject(err);
      });
    });
  };

  const getByIdWithBuku = (id_buku) => {
    return new Promise((resolve, reject) => {
      BukuModel.findOne({
        where: {
          id_buku: id_buku
        },
        include: ['buku']
      }).then(book => {
        resolve(book);
      }).catch(err => {
        reject(err);
      });
    });
  }

  return {
    getById,
    create,
    all,
    allWithPaginate,
    getByIdWithBuku,
    update,
    remove
  }
}

module.exports = BukuService();
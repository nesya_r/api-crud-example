const Promise = require('bluebird').Promise;
const UserService = require('./UserService');
const JwtService = require('./JWTService');

function TokenService() {
  function decodeToken(token) {
    token = token.replace('Bearer ', '');
    let decoded = JwtService.decode(token);
    // console.log(decoded);
    // console.log(Date.now());
    // if (!decoded.exp || (decoded.exp * 1000 <= Date.now())) {
    if (!decoded.exp) {
      let error = Error('Token telah kadaluarsa');
      error.code = 401;
      error.status = 'token_expired';
      throw error;
    } else {
      // should check if `decoded.sub` is present?
      let user = decoded.sub;

      if (!user) {
        let error = new Error();
        error.code = 401;
        error.message = 'Token tidak valid.';
        throw error;
      }
      // check if token is expired
      return new Promise(function (resolve, reject) {
        UserService.getById(user.id)
          .then(function (p) {
            if (p) {
              let user = p.toJSON();
              resolve({
                user: user,
              });
            } else {
              let error = new Error();
              error.code = 401;
              error.message = 'User tidak ditemukan';
              reject(error);
            }
          }, function (error) {

            // error.code = 401;
            // error.message = 'Tidak dapat mengambil data user.';
            reject(error);
          });
      });
    }
  }
  return {
    decodeToken: decodeToken
  };
}

module.exports = TokenService();
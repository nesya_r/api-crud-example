module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      unique: true
    },
    password: DataTypes.STRING,
    full_name: DataTypes.STRING,
    is_delete: DataTypes.BOOLEAN,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    is_active: {
      type: DataTypes.BOOLEAN,
      defaultValue: 1
    }
  }, {
      classMethods: {},
      instanceMethods: {
        toJSON: function () {
          let values = Object.assign({}, this.get());
          delete values.password;
          return values;
        }
      },
      underscored: true,
      paranoid: false
    })
};
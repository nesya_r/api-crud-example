module.exports = function (sequelize, DataTypes) {
    return sequelize.define('book', {
      id_buku: DataTypes.INTEGER,
      nama_buku: DataTypes.STRING,
      is_delete: DataTypes.BOOLEAN,
      penerbit: DataTypes.STRING,
      penulis: DataTypes.STRING,
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: 1
      }
    }, {
        classMethods: {},
        instanceMethods: {
          toJSON: function () {
            let values = Object.assign({}, this.get());
            delete values.id_buku;
            return values;
          }
        },
        underscored: true,
        paranoid: true
      })
  };
function CourierValidator() {

  const check = (req, res, next, validator) => {
    req.check(validator)
    req.getValidationResult().then((result) => {
      if (!result.isEmpty()) {
        let error = new Error('Validation Error');
        error.code = 400;
        error.message = result.array();
        next(error);
      } else {
        next();
      }
    });
  }

  const all = (req, res, next) => {

    const courierAllValidator = {
      page: {
        optional: true,
        isInt: {
          errorMessage: "Page harus bernilai integer"
        }
      },
      limit: {
        optional: true,
        isInt: {
          errorMessage: "Page harus bernilai integer"
        }
      },
      query: {
        optional: true,
      },
    };

    check(req, res, next, courierAllValidator);
  }

  const create = (req, res, next) => {
    const courierCreateValidator = {
      name: {
        notEmpty: true,
        errorMessage: "Name tidak boleh kosong"
      },
      courier_code: {
        optional: true
      }
    }
    check(req, res, next, courierCreateValidator);
  }
  const remove = (req, res, next) => {
    const courierRemoveValidator = {
      id: {
        isInt: {
          errorMessage: "ID harus bernilai integer"
        }
      }
    }
    check(req, res, next, courierRemoveValidator);
  }

  return {
    all,
    create,
    remove
  }
}

module.exports = CourierValidator();
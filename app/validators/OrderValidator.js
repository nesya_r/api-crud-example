function OrderValidator() {

  const all = (req, res, next) => {

    const orderAllValidator = {
      page: {
        optional: true,
        isInt: {
          errorMessage: "Page harus bernilai integer"
        }
      },
      limit: {
        optional: true,
        isInt: {
          errorMessage: "Page harus bernilai integer"
        }
      },
      query: {
        optional: true,
      },
    };

    req.check(orderAllValidator)
    req.getValidationResult().then(function (result) {
      if (!result.isEmpty()) {
        let error = new Error('Validation Error');
        error.code = 400;
        error.message = result.array();
        next(error);
      } else {
        next();
      }
    });
  }

  const create = (req, res, next) => {
    const orderAllValidator = {
      name: {
        notEmpty: true,
        errorMessage: 'Nama tidak boleh kosong'
      },

    };

    req.check(orderAllValidator)
    req.getValidationResult().then(function (result) {
      if (!result.isEmpty()) {
        let error = new Error('Validation Error');
        error.code = 400;
        error.message = result.array();
        next(error);
      } else {
        next();
      }
    });
  }

  return {
    all,
    create
  }
}

module.exports = OrderValidator();
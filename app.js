const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const expressValidator = require('express-validator');
const path = require('path');

const inputAccessor = require('./app/middlewares/InputAccessorMiddleware');

const appRoutes = require('./app/routes/Index');

const config = require('./app/config/config');

const app = express();

// views
app.set('view engine', 'pug');
app.set('views', __dirname + '/app/views');

app.use(bodyParser.json({
  limit: '100mb'
}));
app.use(bodyParser.urlencoded({
  extended: false,
  limit: '100mb'
}));
app.use(express.static(path.join(__dirname, 'public')));

// validation
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
    var namespace = param.split('.'),
      root = namespace.shift(),
      formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param: formParam,
      msg: msg,
      value: value
    };
  }
}));


// handle CORS
var originsWhitelist = [
  'http://localhost:4200',
  'http://localhost:8100',
  'http://localhost',
  'http://inspiralocal.id',
  'http://dashboard.inspiralocal.id',
  'http://flow.idphotobook.com',
  'http://www.flow.idphotobook.com',
  'www.flow.idphotobook.com',
  'http://kirimfoto.idphotobook.com',
  'http://cekfoto.idphotobook.com',
  'http://localhost:8081',
  '107.170.16.218',
  'http://107.170.16.218',
  'https://107.170.16.218',
  'http://dashboard.idphotobook.com',
];
var corsOptions = {
  origin: function (origin, callback) {
    var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
    callback(null, isWhitelisted);
  },
  credentials: true
};

app.use(cors(corsOptions));
app.options('*', cors())
app.use(inputAccessor);

// handle routes
app.use(appRoutes(express));

// handle 404 error
app.use((req, res, next) => {
  let err = new Error('Path Not Found');
  err.status = 404;
  next(err);
});

// handle server error
app.use((err, req, res, next) => {
  let statusCode = err.code;
  if (statusCode >= 100 && statusCode < 600)
    res.status(statusCode);
  else
    res.status(500);
  let message = err.message;
  delete err.message;
  delete err.code;
  res.json({
    status: false,
    message: message,
    data: config.isDevelopment() ? err : {}
  });
});

app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;